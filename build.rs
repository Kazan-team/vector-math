use std::{
    env, fs,
    io::{self, Read, Write},
    path::Path,
    process::{Child, Command, Stdio},
    thread,
};
use vector_math_build_helpers::make_context_types;

fn format_source(source: String) -> String {
    match try_format_source(&source) {
        Ok(v) => v,
        Err(e) => {
            eprintln!("rustfmt failed: {}", e);
            source
        }
    }
}

fn try_format_source(source: &str) -> io::Result<String> {
    let mut command = Command::new("rustfmt")
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()?;
    let stdin = command.stdin.take().unwrap();
    let reader_thread = thread::spawn(move || -> io::Result<(String, Child)> {
        let mut output = String::new();
        command.stdout.take().unwrap().read_to_string(&mut output)?;
        Ok((output, command))
    });
    let write_result = { stdin }.write_all(source.as_bytes());
    let join_result = reader_thread.join().unwrap();
    write_result?;
    let (output, mut command) = join_result?;
    let exit_status = command.wait()?;
    if exit_status.success() {
        Ok(output)
    } else {
        Err(io::Error::new(
            io::ErrorKind::Other,
            format!("exited with status {}", exit_status),
        ))
    }
}

fn main() {
    fs::write(
        Path::new(&env::var_os("OUT_DIR").unwrap()).join("context_trait.rs"),
        format_source(make_context_types().to_string()),
    )
    .unwrap();
}
